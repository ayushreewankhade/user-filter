package com.example.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.UserCount;
import com.example.demo.entity.UserTemp;


@Repository
public interface UserCountRepo extends JpaRepository<UserCount, Long> {

}