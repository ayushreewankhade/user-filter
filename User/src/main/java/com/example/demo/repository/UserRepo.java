package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

//import javax.transaction.Transactional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

//import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>  {

	//@Modifying
	//@Transactional
	//@Query(value= "SEECT * FROM Users ORDER BY name ASC", nativeQuery=true)
	//List<UserDto> findAllByOrderByNameAsc();
	List<User> findAllByOrderByNameAsc();
	
	Optional<User> findByNameContainingIgnoreCase(String name);

	@Query(value= "select * from users u \r\n"
			+ "full outer join user_profile p on p.id=u.id  where   u.is_active= true \r\n"
			+ "AND (:allList= '' OR u.id IN (select unnest(cast(string_to_array(:allList, ',') as bigint[]))))\r\n"
			+ "AND ( u.name ILIKE %:search% OR u.address ILIKE %:search% OR p.role ILIKE %:search%)", nativeQuery= true)
    List<User> findByOrderByIdDesc(@Param ("search") String search, @Param("allList") String allList);

//	select * from users u
//	full outer join user_profile p on p.id=u.id 
//	AND (u.id IN (select unnest(cast(string_to_array('1,2', ',') as bigint[])))) AND u.is_active= true
//	where ( u.name ILIKE '%%' OR u.address ILIKE '%%')
//	@Query(value= "select  * from users  where is_active= true AND(address ILIKE %:search% OR name ILIKE %:search% ) ", nativeQuery= true)
//	List<User> findByOrderByIdDesc(@Param ("search") String search);
}
