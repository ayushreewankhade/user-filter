package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.UserTemp;

public interface UserTempRepo extends JpaRepository<UserTemp, Long> {
	List<UserTemp> getByCountid(Long countid);
}
