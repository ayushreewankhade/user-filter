package com.example.demo.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.config.KafkaProducer;
import com.example.demo.entity.UserCount;
import com.example.demo.entity.UserTemp;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repository.UserCountRepo;
import com.example.demo.repository.UserTempRepo;

@Service
public class UserBulkUploadService {

	@Autowired 
	UserTempRepo userTempRepo;
	
	@Autowired
	KafkaProducer kafkaProducer;
	
	@Autowired 
	UserCountRepo countRepo; 
	
	public void userBulkUpload(XSSFWorkbook workbook,String fileName) {
		List<UserTemp> tempList = new ArrayList<UserTemp>();

		// TODO Auto-generated method stub
		XSSFSheet worksheet = workbook.getSheetAt(0);

		UserCount count = new UserCount();
		count.setFileName(fileName);
		
		countRepo.save(count);
		
		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			UserTemp temp = new UserTemp();

			XSSFRow row = worksheet.getRow(i);
			try {
				temp.setId((long)row.getCell(0).getNumericCellValue());
				temp.setName(row.getCell(1).getStringCellValue());
				temp.setAddress(row.getCell(2).getStringCellValue());
				temp.setCountid(count.getCountid());
				tempList.add(temp);
			} catch (Exception e) {
				throw new ResourceNotFoundException("Data on row " + (i + 1) + " is not in correct format.");
			}

//			try {
//
//				// to avoid duplicate record
//				name = temp.get
//
//				Optional<User> dataBasename = userRepository.findByEmailContainingIgnoreCase(name);
//
//				if ((null == dataBaseEmail) || dataBaseEmail.isEmpty()) {
//
//					this.userRepository.save(entity);
//
//					UserTemp temp2 = temp4.get(j);
//					temp2.setStatus(true);
//					excelRepository.save(temp2);
//
//				} else {
//
//					UserTemp temp2 = temp4.get(j);
//					temp2.setStatus(false);
//					excelRepository.save(temp2);
//
//				}
//
//			} catch (Exception e) {
//
//			}
		}
		userTempRepo.saveAll(tempList);

		saveToUserTable(count.getCountid());
}
	private void saveToUserTable(Long countId) {
		List<UserTemp> list = userTempRepo.getByCountid(countId);
		for (int i = 0; i < list.size(); i++) {
			this.kafkaProducer.addUsersToUsersMainTable(list);
		}
	}

	
}
