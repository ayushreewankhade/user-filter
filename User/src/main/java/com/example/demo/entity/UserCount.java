package com.example.demo.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "userCount")
@Where(clause = "is_active = true")
@SQLDelete(sql = "UPDATE userCount SET is_active=false WHERE id=?")
public class UserCount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long countid;

	private String fileName;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User id;

	@Column(name = "created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "is_active")
	private Boolean isActive = true;

	public Long getCountid() {
		return countid;
	}

	public void setCountid(Long countid) {
		this.countid = countid;
	}

	public User getId() {
		return id;
	}

	public void setId(User id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public UserCount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
