package com.example.demo.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;



@Where(clause = "is_active=true")
@SQLDelete(sql = "UPDATE users SET is_active = false WHERE id=?")
@Entity
@Table(name = "users")
public class User implements Serializable  {

		/**
		 * 
		 */
private static final long serialVersionUID = 1L;
		
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

private String name;

private String address;

private boolean isActive =true; 

@CreationTimestamp
private Date createdAt;

@UpdateTimestamp
private Date updatedAt;

public User() {
	super();
	// TODO Auto-generated constructor stub
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public boolean isActive() {
	return isActive;
}

public void setActive(boolean isActive) {
	this.isActive = isActive;
}

public Date getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
}

public Date getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(Date updatedAt) {
	this.updatedAt = updatedAt;
}

public User(Long id, String name, String address, boolean isActive, Date createdAt, Date updatedAt) {
	super();
	this.id = id;
	this.name = name;
	this.address = address;
	this.isActive = isActive;
	this.createdAt = createdAt;
	this.updatedAt = updatedAt;
}

@Override
public String toString() {
	return "User [id=" + id + ", name=" + name + ", address=" + address + ", isActive=" + isActive + ", createdAt="
			+ createdAt + ", updatedAt=" + updatedAt + "]";
}




	
}
