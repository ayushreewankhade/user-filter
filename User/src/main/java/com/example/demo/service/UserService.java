package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

@Service
public interface UserService {

	User  creatUser(UserDto user);

	void updateUser(UserDto userDto,Integer id);

	UserDto getUserById(Integer userId);

	List<UserDto> getAllUser(String search, String allList);

	public void deleteUser(Integer userId);
	
	List<User> findAllByOrderByNameAsc(); //
}
