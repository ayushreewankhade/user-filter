package com.example.demo.exceptions;

import java.util.Date;
import java.util.NoSuchElementException;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class MyControllerAdvice {
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?>handleResourceNotFoundException
	(ResourceNotFoundException exception,WebRequest request){
	
		ErrorDetails errorDetails=new ErrorDetails(new Date(),exception.getMessage(),request.getDescription(false));
		return new ResponseEntity<>(errorDetails,HttpStatus.NOT_FOUND); 
	}

	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<?>handleNoSuchElementException
	(NoSuchElementException exception,WebRequest request){
	
		ErrorDetails errorDetails=new ErrorDetails(new Date(),exception.getMessage(),request.getDescription(false));
		return new ResponseEntity<>(errorDetails,HttpStatus.NOT_FOUND); 
	}
	
	@ExceptionHandler(EmptyResultDataAccessException.class)
	public ResponseEntity<?>handleEmptyResulDataAccessException
	(EmptyResultDataAccessException exception,WebRequest request){
	
		ErrorDetails errorDetails=new ErrorDetails(new Date(),exception.getMessage(),request.getDescription(false));
		return new ResponseEntity<>(errorDetails,HttpStatus.NOT_FOUND); 
	}
	
}
