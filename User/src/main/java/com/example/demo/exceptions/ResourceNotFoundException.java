package com.example.demo.exceptions;

public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public ResourceNotFoundException(final String message) {
		super(message);
		
	}
}
