package com.example.demo.dto;

public interface IUserDto {

	public int getId();
	public String getName();
	public String getAddress();
	
}
