package com.example.demo.controller;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.ErrorResponseDto;
import com.example.demo.dto.SuccessResponseDto;
import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;
//import com.example.demo.entity.User;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.serviceImpl.UserBulkUploadService;
import com.example.demo.serviceImpl.UserServiceImpl;


@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserServiceImpl userService;
	
	@Autowired
	UserBulkUploadService userBulkUploadService;
	
	@GetMapping("/all")
	public ResponseEntity<?> getAllUser(@RequestParam(defaultValue = "") String search, @RequestParam(defaultValue = "") String allList){
		
		try {
			List<UserDto> user=this.userService.getAllUser(search, allList);

			return new ResponseEntity<>(new SuccessResponseDto("Sucess","Sucess", user),HttpStatus.OK);

		}catch(NoSuchElementException e) 
		{
			return new ResponseEntity<>( new ErrorResponseDto(e.getMessage(),"USER NOT FOUND"),HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getUserById( @PathVariable("id") Integer userId)
	{
		try {

			UserDto userDto=this.userService.getUserById(userId);
			return new ResponseEntity<>(new SuccessResponseDto("Success","Success", userDto),HttpStatus.OK);
		}catch(ResourceNotFoundException e) {
			return new ResponseEntity<>( new ErrorResponseDto(e.getMessage(),"User Not Found"),HttpStatus.NOT_FOUND);
		}	
}
	@PutMapping("/{id}")
	public ResponseEntity<?> updateUser( @RequestBody UserDto userDto,@PathVariable("id")Integer userId )
	{
		try {
			userService.updateUser(userDto, userId);
			return new  ResponseEntity<>("User successfully updated",HttpStatus.OK);
			
		}catch(NoSuchElementException e) {
			return new ResponseEntity<>( new ErrorResponseDto(e.getMessage(),"User Not Found"),HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") Integer userId){
		try {
		this.userService.deleteUser(userId);
			return new ResponseEntity<>(new SuccessResponseDto("Success","Success", "Deleted Ok.!"),HttpStatus.OK); //(HttpStatus.ACCEPTED);
		}
		catch(EmptyResultDataAccessException e){
			return new ResponseEntity<> (new ErrorResponseDto(e.getMessage(),"User Not Found"), HttpStatus.NOT_FOUND);
			
		}
	}

	@PostMapping()
	public ResponseEntity<?> addUser( @RequestBody UserDto userDto) {
		
	
			this.userService.creatUser(userDto);
			return new ResponseEntity<>("User Successfully created",HttpStatus.OK);	
		
	}
		
	@GetMapping("/byName")
    public List<User> get() {
	return this.userService.findAllByOrderByNameAsc();
    }
	
	/*
	 @GetMapping("/byName")
	public ResponseEntity<?> getAllUserByNameAsc(){
		List<UserDto> user=this.userService.findAllByOrderByNameAsc();

		try {
			return new ResponseEntity<>(new SuccessResponseDto("Sucess","Sucess", user),HttpStatus.OK);

		}catch(NoSuchElementException e) 
		{
			return new ResponseEntity<>( new ErrorResponseDto(e.getMessage(),"USER NOT FOUND"),HttpStatus.BAD_REQUEST);
		}
	}*/
	@PostMapping("bulkUpload")
	public ResponseEntity<?> usersBulkUpload(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException {

			XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
try {
	userBulkUploadService.userBulkUpload(workbook, file.getOriginalFilename());
	return new ResponseEntity<>("Users uploaded",HttpStatus.OK);


		} catch (Exception e) {
			return ResponseEntity.ok(new ErrorResponseDto(e.getMessage(), ""));
		}
	}}
	

