import javax.servlet.http.HttpServletRequest;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.serviceImpl.UserBulkUploadService;

public class BulkUploadController {
	
	@Autowired
	UserBulkUploadService userBulkUploadService;
	
	@PostMapping()
	public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file, HttpServletRequest request)
			throws Exception {

		XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
		this.userBulkUploadService.userBulkUpload(workbook,  file.getOriginalFilename());

		return new ResponseEntity<>("file uploaded and save to db", HttpStatus.OK);
		

		}}

